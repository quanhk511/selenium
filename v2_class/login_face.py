import time
from selenium import webdriver

class Face_login:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def accessFb(self):
        self.driver.get(self.url)
        time.sleep(1)

    def getTitle(self):
        print(self.driver.title)

    def insertUser(self, user):
        User = self.driver.find_element_by_id('email')
        User.send_keys(user)

    def insertPasswd(self, pwd):
        Pwd = self.driver.find_element_by_id('pass')
        Pwd.send_keys(pwd)

    def login(self):
        login = self.driver.find_element_by_name('login')
        login.click()
        time.sleep(5)

def Account():
    print("user: ")
    user = input().strip()
    print("pwd: ")
    pwd = input().strip()
    return [user, pwd]

def main():
    driver = webdriver.Chrome()
    url = "https://www.facebook.com/"
    user, pwd = Account()
    FB = Face_login(driver, url)
    FB.accessFb()
    FB.getTitle()
    FB.insertUser(user)
    FB.insertPasswd(pwd)
    FB.login()

if __name__ == "__main__":
    main()
