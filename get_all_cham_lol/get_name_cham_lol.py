import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import argparse

class Get_name_cham:
    def __init__(self, url):
        self.url = url
        self.driver = webdriver.Chrome()

    def accessUrl(self):
        self.driver.get(self.url)
        time.sleep(5)

    def getTitle(self):
        print(self.driver.title)

    def quitBrowser(self):
        print('exit')
        self.driver.quit()

    def getChamLol(self):
        chams = self.driver.find_element_by_css_selector("#champion-grid-content > div > ul")
        print(chams.text)

    def sortChamAd(self):
        print('get chams AD')
        javascript = 'document.querySelector("#role-fighter").click()'
        self.driver.execute_script(javascript)
        time.sleep(1)

    def sortChamAssassin(self):
        print('get chams Assassin')
        javascript = 'document.querySelector("#role-assassin").click()'
        self.driver.execute_script(javascript)
        time.sleep(1)

    def sortChamSupport(self):
        print('get chams Support')
        javascript = 'document.querySelector("#role-support").click()'
        self.driver.execute_script(javascript)
        time.sleep(1)

    def sortChamTank(self):
        print('get chams Tank')
        javascript = 'document.querySelector("#role-tank").click()'
        self.driver.execute_script(javascript)
        time.sleep(1)

    def sortChamMage(self):
        print('get chams Mage')
        javascript = 'document.querySelector("#role-mage").click()'
        self.driver.execute_script(javascript)
        time.sleep(1)

    def getAllCham(self):
        self.accessUrl()
        self.getTitle()
        self.getChamLol()

    def getChamAd(self):
        self.accessUrl()
        self.getTitle()
        self.sortChamAd()
        self.getChamLol()

    def getChamAssassin(self):
        self.accessUrl()
        self.getTitle()
        self.sortChamAssassin()
        self.getChamLol()

    def getChamSupport(self):
        self.accessUrl()
        self.getTitle()
        self.sortChamSupport()
        self.getChamLol()

    def getChamTank(self):
        self.accessUrl()
        self.getTitle()
        self.sortChamTank()
        self.getChamLol()

    def getChamMage(self):
        self.accessUrl()
        self.getTitle()
        self.sortChamMage()
        self.getChamLol()

def main():
    parser = argparse.ArgumentParser(description='input param type cham')
    parser.add_argument('-a', '--all',       dest='command', action='store_const', const='all')
    parser.add_argument('-ad', '--fighter',  dest='command', action='store_const', const='fighter')
    parser.add_argument('-ap', '--mage',     dest='command', action='store_const', const='mage')
    parser.add_argument('-as', '--assassin', dest='command', action='store_const', const='assassin')
    parser.add_argument('-s',  '--support',  dest='command', action='store_const', const='support')
    parser.add_argument('-t',  '--tank',     dest='command', action='store_const', const='tank')
    args = parser.parse_args()

    url = "https://lienminh.garena.vn/game-info/champions"
    lol = Get_name_cham(url)

    if args.command == 'fighter':
        lol.getChamAd()
    elif args.command == 'mage':
        lol.getChamMage()
    elif args.command == 'assassin':
        lol.getChamAssassin()
    elif args.command == 'support':
        lol.getChamSupport()
    elif args.command == 'tank':
        lol.getChamTank()
    elif args.command == 'all':
        lol.getAllCham()

if __name__ == "__main__":
    main()
