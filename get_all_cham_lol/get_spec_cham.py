from get_name_cham_lol import Get_name_cham
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

class Get_spec_cham(Get_name_cham):
    def specAshe(self):
        print('spec Ashe')
        javascript = 'document.querySelector("#champion-grid-22 > div:nth-child(2) > h3 > a:nth-child(2)").click()'
        self.driver.execute_script(javascript)
        specs = self.driver.find_elements_by_css_selector("#toc > div.section-wrapper-content > div.section-wrapper-content-wrapper > div:nth-child(4) > div:nth-child(1) > div > div:nth-child(2) > div > div > div:nth-child(1)")
        for spec in specs:
            stat_ad = spec.find_element_by_css_selector("p:nth-child(3)")
            stat_as = spec.find_element_by_css_selector("p:nth-child(4)")
            print(stat_ad.text)
            print(stat_as.text)
def main():
    url = "https://lienminh.garena.vn/game-info/champions"
    ashe = Get_spec_cham(url)
    ashe.accessUrl()   # inheritance Get_name_cham
    ashe.sortChamAd()  # inheritance Get_name_cham
    ashe.specAshe()
    ashe.quitBrowser() # inheritance Get_name_cham

if __name__ == "__main__":
    main()