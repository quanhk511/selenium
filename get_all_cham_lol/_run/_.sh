SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)
docstring='
usage: PARAM=--all ./_.py
optional arguments:
  -h, --help       show this help message and exit
  -a, --all
  -ad, --fighter
  -ap, --mage
  -as, --assassin
  -s, --support
  -t, --tank

'
[ -z $PARAM ] && (echo 'Envvar PARAM is required' ; kill $$)

    cd $AH
        PYTHONPATH=`pwd` pipenv  run  python  "$AH/get_all_cham_lol/get_name_cham_lol.py" $PARAM
