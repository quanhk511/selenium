from  loginface import getURL, fill_user_pwd, login
import time
from selenium import webdriver

users = ['abc', 'xyz', '123']
pwd = '456'

def main():
    for user in users:
        driver = webdriver.Chrome()
        getURL(driver)
        fill_user_pwd(driver, user, pwd)
        login(driver)
        time.sleep(5)

if __name__ == "__main__":
    main()