import time
from selenium import webdriver


def getURL(driver):
    driver.get("https://www.facebook.com/")
    time.sleep(1)

def getTitle(driver):
    print(driver.title)


def fill_user_pwd(driver, user, pwd):
    User = driver.find_element_by_id('email')
    Pwd = driver.find_element_by_id('pass')
    User.send_keys(user)
    Pwd.send_keys(pwd)

def login(driver):
    login = driver.find_element_by_name('login')
    login.click()

def main():
    user = input().strip()
    pwd = input().strip()
    driver = webdriver.Chrome()
    getURL(driver)
    getTitle(driver)
    User, Pwd = Account(driver)
    fill_user_pwd(User, Pwd, user, pwd)
    login(driver)
    time.sleep(5)

if __name__ == "__main__":
    main()
